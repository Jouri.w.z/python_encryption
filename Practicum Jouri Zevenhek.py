def intro():
    print('This is a program to encrypt or decrypt a message using the Vigenère cypher.')
    print('The Vigenère cypher uses a key word to encrypt the letters of your message.')
    print()

def choice():
    # Asks if the user wants to encrypt or decrypt
    while True:
        print('Do you want to encrypt or decrypt a message? (e or d)')
        answer = input()
        answer = answer.lower()
        if len(answer) != 1:
            print('please type a "e" to encrypt or "d" to decrypt.')
        elif answer not in 'ed':
            print('please type a "e" to encrypt or "d" to decrypt.')
        elif answer == 'e':
            return encrypt()
        elif answer == 'd':
            return decrypt()
        else:
            return answer


def encrypt():
    # Ask user for message
    print('type a message.')
    message = input()
    # print a white line for neatness
    print()

    # ask user for a key
    print('give your key.')
    key = input()
    # print for a new line.
    print()

    # create a range with the length of the message
    ran = range(len(message))

    # results are gathered in this list
    results = []

    # Iterate though the range and therefor show all the letters
    for i in ran:
        # get current letters for this iteration
        currentLetter = message[i]
        currentKeyLetter = key[i % len(key)]

        # Get corresponding numbers
        numberLetter = ord(currentLetter)
        numberKeyLetter = ord(currentKeyLetter)

        # Add two letters
        sumTwoLetters = numberLetter + numberKeyLetter

        # Get the number of the encrypted letter
        newNumberLetter = sumTwoLetters % 128
        # Get the encrypted number based on number
        newLetter = chr(newNumberLetter)

        # The results are then added to the end of the list.
        results.append(newLetter)

    # Joins the message together to remove the ''
    print('encrypted message: ' + "".join(results))

def decrypt():
    # Ask user for message
    print('type a message.')
    message = input()
    # print a white line for neatness
    print()

    # ask user for a key
    print('give your key.')
    key = input()
    # print for a new line.
    print()

    # create a range with the length of the message
    ran = range(len(message))

    # results are gathered in this list
    results = []

    # Iterate though the range and therefor show all the letters
    for i in ran:
        # get current letters for this iteration
        currentLetter = message[i]
        currentKeyLetter = key[i % len(key)]

        # Get corresponding numbers
        numberLetter = ord(currentLetter)
        numberKeyLetter = ord(currentKeyLetter)

        # subtract two letters
        subTwoLetters = numberLetter - numberKeyLetter

        # Get the number of the decrypted letter
        newNumberLetter = subTwoLetters % 128
        # Get the decrypted number based on number
        newLetter = chr(newNumberLetter)

        # the results are then added to the end of the list.
        results.append(newLetter)

    # Joins the message together to remove the ''
    print('decrypted message: ' + "".join(results))

def restart():
    while True:
        print()
        print('Do you want to start over again?')
        print('type "y" restart or type "n" to stop')
        answer = input()
        answer = answer.lower()
        if len(answer) != 1:
            print('Please type a "y" start again or "n" to stop')
        elif answer not in 'yn':
            print('Please type a "y" start again or "n" to stop')
        elif answer == 'y':
            return choice()
        elif answer == 'n':
            break



intro()
choice()
restart()